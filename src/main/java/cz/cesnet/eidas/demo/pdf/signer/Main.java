package cz.cesnet.eidas.demo.pdf.signer;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureOptions;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Calendar;

/**
 * Demonstrates signing pdf document by calculating hash and using PKCS7 RemSig API
 *
 * Global variables need to be set up correctly first.
 *
 * For simplicity, sealing is used instead of singing. Signing requires extra manual step (password entry).
 * Sealing instead supports password entry in request body and http response contains raw bytes.
 */
public class Main {
    // the RemSig base API url (note: no / at the end)
    // development instance: https://remsig-dev.cesnet.cz/api
    //  production instance: https://app.remsig.cesnet.cz/api
    private static final String API_URL = "https://remsig-dev.cesnet.cz/api";

    // the system http authentication
    private static final String HTTP_BASIC_AUTH_USER = "username";
    private static final String HTTP_BASIC_AUTH_PASS = "password";

    // the user and password
    private static final String UNIQUE_ID = "personid@my.organization";
    private static final String PASSWORD = "password";

    // document and signature properties
    private static final String PDF_DOCUMENT_SOURCE = "sample.pdf";
    private static final String PDF_DOCUMENT_RESULT = "signed.pdf";
    private static final String HASH_ALGORITHM = "SHA256";

    // Name of external timestamp server
    // set to null for no external timestamp (local RemSig server time will be used)
    // You can get list of available server names by GET request on /timestamp/servers
    // "tsa_cesnet" timestamp server is available both on development and production instances
    private static final String TIMESTAMP_SERVER = "tsa_cesnet";

    public static void main(String[] args) throws Exception {
        var document = loadDocument();

        // create signature dictionary
        var signature = new PDSignature();
        signature.setFilter(PDSignature.FILTER_ADOBE_PPKLITE);
        signature.setSubFilter(PDSignature.SUBFILTER_ADBE_PKCS7_DETACHED);
        signature.setSignDate(Calendar.getInstance());
        document.addSignature(signature, null, new SignatureOptions());

        var outputFile = new File(PDF_DOCUMENT_RESULT);
        try (var os = new FileOutputStream(outputFile)) {
            // set external signing
            var externalSigning = document.saveIncrementalForExternalSigning(os);
            // invoke external signature service
            byte[] cmsSignature = createCMSSignatureInRemSig(externalSigning.getContent());
            // set signature bytes received from the service
            externalSigning.setSignature(cmsSignature);

            document.close();

            System.out.println("Signed document available at " + outputFile.getAbsolutePath());
        }
    }

    static PDDocument loadDocument() throws IOException {
        try (var is = Main.class.getClassLoader().getResourceAsStream(PDF_DOCUMENT_SOURCE)) {
            return PDDocument.load(is);
        }
    }

    static byte[] createCMSSignatureInRemSig(InputStream content) throws Exception {
        // calculate digest
        var md = MessageDigest.getInstance(HASH_ALGORITHM, new BouncyCastleProvider());
        byte[] digest = md.digest(IOUtils.toByteArray(content));

        // send digest to RemSig
        return callSealPkcs7API(digest);
    }

    static byte[] callSealPkcs7API(byte[] dataToSign) throws Exception {
        var dataToSignString = Base64.getEncoder().encodeToString(dataToSign);

        var authorizationHeader = "Basic " + Base64.getEncoder().encodeToString(
                (HTTP_BASIC_AUTH_USER + ":" + HTTP_BASIC_AUTH_PASS).getBytes(StandardCharsets.UTF_8));

        var timestampJsonPart = "";
        if (TIMESTAMP_SERVER != null) {
            timestampJsonPart = "\"timestamp\": { \"name\": \"" + TIMESTAMP_SERVER + "\"},";
        }

        var requestBodyJson = "{" +
                "\"personUniqueId\": \"" + UNIQUE_ID + "\"," +
                "\"password\": \"" + PASSWORD + "\"," +
                "\"algorithm\": \"" + HASH_ALGORITHM + "withRSA\"," +
                "\"data\": { \"bytes\": \"" + dataToSignString + "\" }," +
                timestampJsonPart +
                "\"pem\": false," +
                "\"noDetach\": false," +
                "\"hashData\": false" +
                "}";

        var request = HttpRequest.newBuilder()
                .uri(URI.create(API_URL + "/seal/pkcs7"))
                .header("Content-Type", "application/json")
                .header("Accept", "application/octet-stream, application/json")
                .header("Authorization", authorizationHeader)
                .method("POST", HttpRequest.BodyPublishers.ofString(requestBodyJson))
                .build();

        var response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofByteArray());

        if (response.statusCode() != 200) {
            throw new Exception("Could not sign PKCS7 data. Http response code: " + response.statusCode());
        }

        return response.body();
    }
}
